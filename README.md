# Assembled Checks

Provides "All in one place" HTML reports and badges for python tests, linters and static type checkers

## Project requirements
1. Tools
    1. Flake8
        1. Bandit
        2. Builtins
        3. Docstrings
        4. rst-Docstrings
        5. Isort
    2. MyPy
    3. Pytest
        1. Coverage
        2. Benchmark
    4. Black
    5. Isort
2. Checks
    1. Allow to keep configuration for all tools in one place (pyproject.toml?) and generate subfiles for every check run
    2. Allow to keep generated configs, or to generate them w/o running checks
    3. Allow to use default config
    4. Allow to run tools separatly, or use only-format, only-static-check, all-checks etc.
    5. Allow user to create tools groups 
3. Report
    1. Report Flake8 errors, show place in code, description popup
    2. Report MyPy coverage, show errors in code
    3. UnitTests coverage
    4. Show function benchmark if enabled
    5. Keeping results of previous checks to compare them with each other
    6. Draw statistic charts
    7. Allow branch comparing
    8. Generate badges
