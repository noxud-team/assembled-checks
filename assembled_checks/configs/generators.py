"""Tools configs generators."""


class AbstractGenerator:
    pass


class Flake8ConfigGenerator(AbstractGenerator):
    pass


class MyPyConfigGenerator(AbstractGenerator):
    pass


class CoverageConfigGenerator(AbstractGenerator):
    pass
